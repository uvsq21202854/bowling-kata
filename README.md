Exercice 2.2 (The Bowling Game Kata)

Dans cet exercice, vous vous familiariserez avec le développement dirigé par les tests (Test Driven Development
ou TDD). Pour cela, vous suivrez les étapes du document The Bowling Game Kata, Robert C.
Martin. Une copie au format PDF du document est disponible sur e-campus.

* Effectuez les différentes étapes du document.
* Adaptez les tests unitaires à la version de JUnit du projet.
* Validez les changements après chaque test réussi.
* À la fin, n’oubliez pas de synchroniser le projet sur la forge.